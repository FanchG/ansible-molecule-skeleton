Skeleton to add molecule test to your ansible role
==================================================


## Example with Ansible Discourse

### Get the Role source & create an explicit branch:

```bash
git clone https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-discourse.git
cd ansible-discourse
git checkout -b use_molecule_for_test
cd -
```

*we should never use master branch for work*

### Get the molecule skeleton & remove .git

```bash
git clone git@gitlab.com:FanchG/ansible-molecule-skeleton.git
cd ansible-molecule-skeleton
rm -rf .git LICENSE
cd -
```

*we want to keep our role .git & License, so to prevent any mistake we remove it from our skeleton*

### Copy skeleton to Role source

```bash
rsync -a ansible-molecule-skeleton/ ansible-discourse/
```

*trailing slash are mandatory to avoid creating an additional directory level at the destination.*

### Edit molecule playbook

In [molecule/default/playbook.yml](molecule/default/playbook.yml)

Replace:

```yaml
  tasks:
    - name: Print Information
      debug: msg='You should replace this by a call to your role name'
```

By your role:

```yaml
  roles:
    - role: discourse
```

### Add Requirements

If needed, add a file: molecule/default/requirements.yml

```yaml
- src: geerlingguy.git
- src: geerlingguy.docker
- src: geerlingguy.pip
```

### Commit your change

```bash
cd ansible-discourse
git status
git add .travis.yml .gitlab-ci.yml molecule
git commit -m 'Add basic molecule test'
cd -
```

### Remove ansible from your current path

```bash
cd ansible-discourse
NAME=$(basename $(pwd)) ROLENAME=$(echo $NAME | sed -e s/ansible-//g) && cd .. &&  ln -fs $NAME $ROLENAME && cd $ROLENAME && pwd
cd -
```
*As we want to run it as discourse*


### Run molecule lint

```bash
cd ansible-discourse
molecule lint
cd -
```
*we will have some file to edit before go further, error should be explicit like: "too many spaces before colon"*
*we can ignore some ansible lint error adding the "# noqa 305" pragma where the number is the ansible lint error number*

### Commit your lint fix

```bash
cd ansible-discourse
git commit defaults/main.yml meta/main.yml molecule/default/requirements.yml tasks/discourse.yml -m 'fix for yamlint'
cd -
```

### Remove test on CentOS

In [molecule/default/molecule.yml](molecule/default/molecule.yml)

Remove:
```yaml
  - name: centos-${INSTANCE:-instance}
    [...]
    tmpfs:
      - /tmp
      - /run
```

*As discourse role is not intended to be run on CentOS*

### Run molecule test

```bash
cd ansible-discourse
molecule test
cd -
```
*here we will have a lot of thing to fix ;)*

#### if needed, connect to the molecule docker instance

```
cd ansible-discourse
molecule test --destroy=never
docker exec -it ubuntu-instance /bin/bash
# check any thing
# then exit
molecule destroy;
cd -
```

#### if needed, disable idempotence

In [molecule/default/molecule.yml](molecule/default/molecule.yml)

Add:
```yaml
scenario:
  name: default
  test_sequence:
    - lint
    - cleanup
    - destroy
    - dependency
    - syntax
    - create
    - prepare
    - converge
    # - idempotence
    - side_effect
    - verify
    - cleanup
    - destroy
```
*yes it is needed to add the test sequence to be able to disable one step...*


##### Commit your change

```bash
cd ansible-discourse
git status
git commit molecule/default/molecule.yml -m 'disable idempotence for molecule'
cd -
```

#### if needed, ugly hack docker file system

In [molecule/default/molecule.yml](molecule/default/molecule.yml)

Add:
```yaml
     tmpfs:
       [...]
       - /var/lib/docker
```

*as discourse is run in docker, and we already are in docker, and docker does not allow to mount overlayfs on overlayfs, so we use tmpfs*


### Adapt test to fit need

In [molecule/default/tests/test_default.yml](molecule/default/tests/test_default.yml)

Add:
```yaml
service:
  docker:
    running: true
    enabled: true
```

*Test are written in [Goss](https://github.com/aelsabbahy/goss) but ansible syntax should be [availlable soon](https://github.com/ansible/molecule/issues/2013)*

#### Run molecule test again

```bash
cd ansible-discourse
molecule test
cd -
```


### Push everyting to remote repository


```bash
cd ansible-discourse
git push -u origin use_molecule_for_test
cd -
```

And check the merge if the pipeline on gitlab have run well (or check travis if you are on github)


### Merge request example used in this README.md

https://gitlab.com/ifb-elixirfr/cluster/ansible/merge_requests/306
